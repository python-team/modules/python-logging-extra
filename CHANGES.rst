0.20120214
----------

- 'ignore' utf-8 errors on smtp messages


0.20120104
----------

- Small API change in JabberHandler.
- new unittest/example


.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
