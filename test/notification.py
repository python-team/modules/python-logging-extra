#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import logging
from unittest import TestCase

import loggingx

hooks = [
    (logging.Logger.debug, "debug"),
    (logging.Logger.info, "info"),
    (logging.Logger.warn, "warn"),
    (logging.Logger.error, "error"),
    (logging.Logger.critical, "critical")]

destination = 'target-account@gmail.com'

sender = loggingx.Account(
    username = 'your-account@gmail.com',
    password = 'your-password')

gmail_smtp = loggingx.Server(
    hostname = 'smtp.gmail.com',
    port = 587,
    ssl = True)


class TestLogginx(TestCase):
    def setUp(self):
        self.logger = logging.getLogger('test')
        self.logger.setLevel(logging.DEBUG)

    def tearDown(self):
        for h in self.logger.handlers:
            self.logger.removeHandler(h)

    def send_issues(self):
        for func, msg in hooks:
            func(self.logger, msg)

    def test_desktop(self):
        self.logger.addHandler(loggingx.NotifyHandler())
        self.send_issues()

    def test_jabber(self):
        self.logger.addHandler(
            loggingx.JabberHandler(
                sender = sender,
                to     = destination))

        self.send_issues()

    def test_smtp(self):
        self.logger.addHandler(
            loggingx.SMTP_SSLHandler(
                mailhost    = gmail_smtp.socket,
                fromaddr    = sender.username,
                toaddrs     = [destination],
                subject     = 'SSL SMTP notification',
                credentials = sender.credentials,
                ssl         = gmail_smtp.ssl))

        self.send_issues()


# run with: "nose test/notificacion.py" from parent directory
