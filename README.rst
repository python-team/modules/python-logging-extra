
python-logging-extra is available as official Debian package.

Debian page
===========

- binary: http://packages.debian.org/sid/python-loggingx
- source: http://packages.debian.org/source/sid/python-logging-extra

Source package repository
=========================

- http://http://anonscm.debian.org/viewvc/python-modules/packages/python-logging-extra/
- svn+ssh://${ALIOTH_USER}@svn.debian.org/svn/python-modules/packages/python-logging-extra/


.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
